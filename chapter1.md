Syber woke up to the blaring sound of the ship's alarms.

The ship's public address speakers announce that the ship is critically damaged by meteor impact

There's like a big hole in the side of the ship

There's fire... in space... which is like a plasma ball

The fire is spreading to the inner hull of the ship

The automatic bulkhead doors close to seal off the damaged sector

Many Bird crew get blasted into space by the compressed environment spilling out into the cold darkness of space

Bird crew desperately attempt to stay in the ship as their talons grasp onto the nearest object. It is futile, and the birds are blowin into the abyss clutching bedsheets, broken girders, and other debis.

Luckily, Syber is not in the sector which has lost compression, although the sounds of the destructive chaos can be heard.

Syber throws his shiny space covers off his body, and rises to his feet-talons.

He's in his underwear, and he grabs his uniform from his closet.

His roomate in teh bunk above turns and looks from under her space covers. The feathers on her head are ruffled.

> What's going on?!

Krystal Kavés chirps, half-awake

> I can't believe you slept through that. The ship has been struck by a meteor. It's venting atmosphere, get teh fuck up!

The two roomates go to the corridor and jog towards the sound of the damage.

As they appreach a 90 degree turn, a corparal appears and hastily beckons.

> Stryle, Kavés, get the hell over here!

They get to the copreal and make the turn. A locked bulkhead enters their view, revealing J. "Jung" Aufta with his leg crushed beneath the massive door.

> Ahhhhh! My leg!

Jung writhed in pain as his pinned leg bleed profusely. It was obvious from the door's weight that the bone had been completely crushed.

> Holy shit!

Stryke explaims. Kaves holds back vomit.

The corporal

> We gotta get this door open! Give me a hand!

The corporal grabs an iron rod, debris from the damaged ship, and uses it as a prying tool to try and open the door.

Kaves wipes her beak and jumps in to help, grabbing the end of the rod and applying force downwards.

Stryke joins the two, prying the door with the rudimentary tool.

The three apply dowward force, bracing themselves against the walls to get better leverage.

The door doesn't budge.

> These doors are magnetically sealed! It's no use!

"Jung" wimpers.

The three struggling birds wipe sweat from their brow.

> He's right, I remember airlock training. These things won't open unless you've got the worlds' greatest hydraulic lift.

Kavés added her two cents.

Just then, the lights in the corridor began to flash red.

> DANGER. Oxygen levels depleted in section 5D. Evacuate to 5A immediately. Danger. Oxygen levels...

the message was on a loop.

> Shit! We gotta move, now!

`@decision option to leave Jung`

> Ahh, I knew I would die alone!

Jung cried.

> We can't just leave him!

> He's coming with us, give me that rod.

Stryke firmly grasps the rod, holding it vertically, with wedge side down.

> Jung, grit your teeth!

Stryke thrusts the rod downwards towards Jung's talon-leg, severing it.

> AHHHHHHHHHHHHHHHHHHHHHHHHHHH!!!!

Jung cried out in pain.

The three healthy birds dragged Jung, and they all hobbled to the section of the ship which still had a supply of atmosphere.
